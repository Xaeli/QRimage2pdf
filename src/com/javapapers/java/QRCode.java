package com.javapapers.java;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import javax.imageio.ImageIO;

public class QRCode { 
    public String readQRCode(String filePath, String charset, Map hintMap)
			throws FileNotFoundException, IOException, NotFoundException {
		BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(
				new BufferedImageLuminanceSource(
                                        cropImage(
						ImageIO.read(new FileInputStream(filePath))
                                        )
                                )));
		Result qrCodeResult = new MultiFormatReader().decode(binaryBitmap,
				hintMap);
                
		return qrCodeResult.getText();
	}
        
    private BufferedImage cropImage(BufferedImage src){
        int w= src.getWidth();
        int h= src.getHeight();
        BufferedImage dest = src.getSubimage(w/2,0,w/2,h/3);
        
        return dest;
    }
    
    public String readQRCode(BufferedImage image, String charset, Map hintMap)
			throws FileNotFoundException, IOException, NotFoundException {
		BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(
				new BufferedImageLuminanceSource(image)));
		Result qrCodeResult = new MultiFormatReader().decode(binaryBitmap,
				hintMap);
                
		return qrCodeResult.getText();
	}
        
 
}
