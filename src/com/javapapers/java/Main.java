/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javapapers.java;

import Views.Controller;
import Views.Response;
import com.google.zxing.EncodeHintType;
import com.google.zxing.NotFoundException;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import org.ghost4j.document.DocumentException;
import org.ghost4j.renderer.RendererException;

/**
 *
 * @author Jorge
 */
public class Main implements Controller{
    
    private final String charset; //"UTF-8" or "ISO-8859-1"
    private final Map<EncodeHintType, ErrorCorrectionLevel> hintMap;
    private final QRCode qrc;
    private final PdfToImage pdf;
    private ArrayList<String> codes;
    private Map<String,File> archivos;
    private Map<String, BufferedImage> images;
    private final Response viewUpdate;
    private int good=0,bad=0;
    private File imgDir;
//    ArrayList<File> localFiles;
    
    public Main(Response r){
        this.viewUpdate=r;
        qrc = new QRCode();
        pdf = new PdfToImage();
        codes = new ArrayList<>();
        charset = "UTF-8";
        imgDir = new File("images");
        createImgDir(imgDir);
        hintMap = new EnumMap<>(EncodeHintType.class);
		hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
                
    }
   
    private void createImgDir(File img){
        if(!img.exists()){
            try{
                img.mkdir();
            }
            catch(SecurityException ex){
                viewUpdate.update("Ocurrio un error al crear el directorio");
            }
        }
    }

    @Override
    public void getImageFiles(File[] files) {
        getImageText(files);
    }

    @Override
    public void getPdfFile(File file, int resolution) {
        try {
            //se obtiene el nombre del archivo
            String fileName = file.getName();
            fileName = fileName.substring(0, fileName.length()-4);
            //Convierte el pdf a imagen
            pdfToImage(file.getAbsolutePath(),fileName, resolution);
            //Busca el codigo dentro de todo el texto obtenido
            ImgRegExp("Aa\\d+",fileName);
            //Imprime en consola
            show();
        }
        catch (IOException | RendererException | DocumentException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

        
       
    }

    @Override
    public void applyRegEx(String match) {
        RegExp(match);
        deleteGoodOnes();
    }

    @Override
    public void showInScreen() {
        show();
    }
    
    @Override
    public void reset() {
        codes.clear();
        archivos.clear();
        images.clear();
        good=0;
        bad=0;
        
    }
    
    
    private void pdfToImage(String path,String filename, int resolution) throws IOException, RendererException, DocumentException{
        BufferedImage[] pngs;
        images = new HashMap<>();
        
        pngs=pdf.convert(path,resolution);
        
        good=0;
        bad=0;
        for(BufferedImage img:pngs){
            
            try {
                String res=qrc.readQRCode(img, charset, hintMap);
                images.put(res,img);
                good++;
            }
            catch (NotFoundException ex) {
                //indice++;
                bad++;
                viewUpdate.update("No se encontró código QR en la imagen\n");
                createImgDir(imgDir);
                ImageIO.write((RenderedImage) img, "png", new File("images/"+filename+"-"+(bad) + ".png"));
            }
        }
        viewUpdate.update(
                "Total: "+pngs.length+"\n"
                + "Correctas:"+good+"\n"
                + "Incorrectas:"+bad);
            
     
    }
    
    private void ImgRegExp(String match, String filename) throws IOException{
      Pattern pattern = Pattern.compile(match);
      Iterator ite = images.keySet().iterator();
      
      while(ite.hasNext()){
          String key = (String)ite.next();
//          System.out.println(key);
          
          Matcher matcher = pattern.matcher(key);
            try{
                matcher.find();
                String res=matcher.group();
                codes.add(res);
            }
            catch(IllegalStateException ex){
                String update="No se encuentra la expresión\n";
                createImgDir(imgDir);
                bad++;
                ImageIO.write((RenderedImage) images.get(key), "png", new File("images/"+filename+"-"+(bad) + ".png"));
                viewUpdate.update(update);
                ite.remove();
                
            } 
      }
      viewUpdate.update("Incorrectas:"+bad);
    }
    
    private void export(String path) throws IOException{
        
//        path+="\\output.txt";
        path="output.txt";
        //System.out.println("save path "+path);
        FileWriter writer = new FileWriter(path); 
        for(String str: codes) {
          writer.write(str);
           writer.write(System.getProperty( "line.separator" ));
        }
        writer.close();
    }
    
    private void show(){
        viewUpdate.update("\n");
        for(String str:codes){
            viewUpdate.update(str);
        }
    }
    
    private void getImageText(File[] files){
        archivos=new HashMap<>();
        good=0;
        bad=0;
        for(File f:files){
            try {
                String res=qrc.readQRCode(f.getAbsolutePath(), charset, hintMap);
                archivos.put(res,f);
                good++;
            } catch (IOException ex) {
                viewUpdate.update("Archivo no pudo ser leido");
                bad++;
            } catch (NotFoundException ex) {
                bad++;
                viewUpdate.update("No se encontró código QR en el archivo:\n"+f);
            }
        }
        viewUpdate.update(
                "Total: "+files.length+"\n"
                + "Correctas:"+good+"\n"
                + "Incorrectas:"+bad);
    }
    
    
    
    private void RegExp(String match){
      Pattern pattern = Pattern.compile(match);
      Iterator ite = archivos.keySet().iterator();
      
      while(ite.hasNext()){
          String key = (String)ite.next();
//          System.out.println(key);
          
          Matcher matcher = pattern.matcher(key);
            try{
                matcher.find();
                String res=matcher.group();
                codes.add(res);
            }
            catch(IllegalStateException ex){
                String update="No cuadra con la expresión:\n"
                        + archivos.get(key)+" - "+key;
                viewUpdate.update(update);
                ite.remove();
                bad++;
            } 
      }
      viewUpdate.update("Incorrectas:"+bad);
    }
    
    private void deleteGoodOnes(){
        
        int undelete=0;
        
            Iterator ite = archivos.keySet().iterator();
            while(ite.hasNext()){
                String key = (String)ite.next();
                archivos.get(key).setWritable(true);
//                if(archivos.get(key).delete()){
//                    ite.remove();
//                }
//                else{
//                    ++undelete;
//                    viewUpdate.update("No se pudo eliminar hasta salir:\n"+archivos.get(key));
//                    archivos.get(key).deleteOnExit();
//                }
            }
            System.out.println("");
        
    }

    
    
}
