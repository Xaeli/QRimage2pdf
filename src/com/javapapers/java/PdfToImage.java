/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javapapers.java;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.List;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import org.ghost4j.document.DocumentException;
import org.ghost4j.document.PDFDocument;
import org.ghost4j.renderer.RendererException;
import org.ghost4j.renderer.SimpleRenderer;


/**
 *
 * @author Jorge
 */
public class PdfToImage {
    
    public BufferedImage[] convert(String path, int resolution) throws IOException, RendererException, DocumentException{
        ArrayList<Image> images=null;
        BufferedImage[] pngs;
     
 
         // load PDF document
         PDFDocument document = new PDFDocument();
         document.load(new File(path));
 
         // create renderer
         SimpleRenderer renderer = new SimpleRenderer();
 
         // set resolution (in DPI)
         renderer.setResolution(resolution);
 
         // render
         
            images = (ArrayList<Image>) renderer.render(document);

            
         // write images to files to disk as PNG
//            try {
//                for (int i = 0; i < images.size(); i++) {
//                    ImageIO.write((RenderedImage) images.get(i), "png", new File((i + 1) + ".png"));
//                }
//            } catch (IOException e) {
//                System.out.println("ERROR: " + e.getMessage());
//            }
        
        
        
 
        
        
        pngs = new BufferedImage[images.size()];
        
        for(int i=0; i<pngs.length; i++){
            pngs[i]=toBufferedImage(images.get(i));
        }
        
        
        return pngs;   
    }
    
    private BufferedImage toBufferedImage(Image img){
        if (img instanceof BufferedImage){
            return (BufferedImage) cropImage((BufferedImage)img);
        }

        // Create a buffered image with transparency
        BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

        // Draw the image on to the buffered image
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();

        // Return the buffered image
        return cropImage(bimage);
    }
    
    private BufferedImage cropImage(BufferedImage src){
        int w= src.getWidth();
        int h= src.getHeight();
        BufferedImage dest = src.getSubimage(w/2,0,w/2,h/3);
        
        return dest;
    }
    
}
