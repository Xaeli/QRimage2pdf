/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import java.io.File;

/**
 *
 * @author Jorge
 */
public interface Controller {
    

    public void getImageFiles(File[] files);
    public void getPdfFile(File file, int resolution);
    public void applyRegEx(String match);
    public void showInScreen();
    public void reset();
}
